<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_interests', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('reservation_id');
			$table->foreign('reservation_id')->references('id')->on('reservations')->onDelete('cascade');
			$table->string('name');
			$table->string('phone');
			$table->string('email');
			$table->boolean('visualized')->default(false);
			$table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservation_interests', function(Blueprint $t) {
			$t->dropForeign('reservation_interests_reservation_id_foreign');
			$t->dropIfExists();
		});
    }
}
