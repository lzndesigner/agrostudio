<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('page_description');
            $table->string('page_image');
            $table->string('box_vision_title');
            $table->text('box_vision_description');
            $table->string('box_mission_title');
            $table->text('box_mission_description');
            $table->string('box_quality_one_title');
            $table->text('box_quality_one_description');
            $table->string('box_quality_two_title');
            $table->text('box_quality_two_description');
            $table->string('box_quality_three_title');
            $table->text('box_quality_three_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
