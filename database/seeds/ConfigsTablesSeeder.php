<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Config;

class ConfigsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->delete();

        Config::create([
            'config_title' => 'AgroStudio',
            'config_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'config_keywords' => 'agrostudio, agro, studio,',
            'config_email' => 'contato@agrostudio.com.br',
            'config_phone' => '(00) 0000-0000',
            'config_cellphone' => '(00) 0000-0000',
            'config_information' => 'Informações Complementares',
            'company_name' => 'AgroStudio',
            'company_proprietary' => 'André Evangelista',
            'company_address' => 'Av. Francisco Junqueira, 550',
            'company_city' => 'Ribeirão Preto',
            'company_state' => 'São Paulo',
            'company_country' => 'BR',
        ]);
    }
}
