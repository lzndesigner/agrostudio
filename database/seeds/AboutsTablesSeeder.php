<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\About;

class AboutsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->delete();

        About::create([
            'page_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'page_image' => 'sem_image.png',
            'box_vision_title' => 'Nossa Visão',
            'box_vision_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'box_mission_title' => 'Nossa Missão',
            'box_mission_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'box_quality_one_title' => 'Qualidade',
            'box_quality_one_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'box_quality_two_title' => 'Atendimento',
            'box_quality_two_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'box_quality_three_title' => 'Comprometimento',
            'box_quality_three_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
        ]);
    }
}