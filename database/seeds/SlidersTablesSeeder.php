<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Slider;

class SlidersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->delete();

        Slider::create([
            'title'=>'Somos referência em',
            'subtitle'=>'Regulamentação Florestal',
            'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, repellat doloremque, ut temporibus maiores odio?',            
            'url'=>'#',
            'image'=>'/sliders/sem_image.png',
        ]);
    }
}

