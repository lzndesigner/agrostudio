<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsletter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:newsletters',
            'phone' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'nome',
            'phone' => 'telefone/celular',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Este e-mail ja consta em nossa lista!'
        ];
    }
}
