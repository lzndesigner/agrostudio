<?php

namespace App\Http\Requests\Admin\About;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'page_description' => ['required', 'min:4'],
            'box_vision_title' => ['required', 'min:4'],
            'box_vision_description' => ['required', 'min:4'],
            'box_mission_title' => ['required', 'min:4'],
            'box_mission_description' => ['required', 'min:4'],
            'box_quality_one_title' => ['required', 'min:4'],
            'box_quality_one_description' => ['required', 'min:4'],
            'box_quality_two_title' => ['required', 'min:4'],
            'box_quality_two_description' => ['required', 'min:4'],
            'box_quality_three_title' => ['required', 'min:4'],
            'box_quality_three_description' => ['required', 'min:4'],
        ];
    }

    public function messages()
        {
            return [
                'page_description.required' => 'A Descrição da Página é necessário.',
                'box_vision_title.required' => 'O Título da Box de Visão é necessário.',
                'box_vision_description.required' => 'A Descrição da Box de Visão é necessário.',
                'box_mission_title.required' => 'O Título da Box de Missão é necessário.',
                'box_mission_description.required' => 'A Descrição da Box de Missão é necessário.',
                'box_quality_one_title.required' => 'O Título da Box 1 de Qualidade é necessário.',
                'box_quality_one_description.required' => 'A Descrição da Box 1 de Qualidade é necessário.',
                'box_quality_two_title.required' => 'O Título da Box 2 de Qualidade é necessário.',
                'box_quality_two_description.required' => 'A Descrição da Box 2 de Qualidade é necessário.',
                'box_quality_three_title.required' => 'O Título da Box 3 de Qualidade é necessário.',
                'box_quality_three_description.required' => 'A Descrição da Box 3 de Qualidade é necessário.',
            ];
        }
}
