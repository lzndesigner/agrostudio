<?php

namespace App\Http\Requests\Admin\Reservations;

use Illuminate\Foundation\Http\FormRequest;

class ReservationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
			 'propietario' => 'required',
			 'fazenda' => 'required',
			 'matricula' => 'required',
			 'municipio_cartorio' => 'required',
			 'municipio' => 'required',
			 'estado' => 'required',
			 'bioma' => 'required|in:Cerrado,Caatinga,Amazônia,Mata Atlântica,Pantanal,Pampa',
			 'situacao' => 'required|in:regular,irregular',
			 'georeferencia' => 'required|in:sim,nao',
			 'inscricao_car' => 'nullable',
			 'tamanho_area' => 'required',
			 'area_tipo' => 'required|in:hectares,alqueires',
        ];
    }

    public function attributes() {
        return [
			'municipio_cartorio' => 'Cartório do Município'
        ];
    }
    
    public function messages()
        {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
            return [
            ];
        }
}
