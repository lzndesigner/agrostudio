<?php

namespace App\Http\Requests\Admin\Config;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'config_title' => ['required', 'min:4'],
            'config_description' => ['required', 'min:4', 'max:255'],
            'config_email' => ['required'],
            'config_phone' => ['required'],
            'company_name' => ['required'],
            'company_proprietary' => ['required'],
            'company_city' => ['required'],
            'company_state' => ['required'],
            'company_country' => ['required'],
        ];
    }

    public function messages()
        {
            return [
                'config_title.required' => 'O Título é necessário.',
                'config_description.required' => 'A Descrição é necessária.',
                'config_email.required' => 'O E-mail é necessário.',
                'config_phone.required' => 'O Telefone é necessário.',
                'company_name.required' => 'O Nome do Site é necessário.',
                'company_proprietary.required' => 'O Nome do Proprietário é necessário.',
                'company_city.required' => 'A Cidade é necessário.',
                'company_state.required' => 'O Estado é necessário.',
                'company_country.required' => 'O País é necessário.',
            ];
        }
}
