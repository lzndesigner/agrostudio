<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\About\StoreRequest;
//use App\Http\Requests\Admin\About\UpdateRequest;

use App\Models\About;

class AboutController extends Controller
{
    /**
     * Armazena uma nova instancia do model About
     *
     * @var \App\About
     */
    private $abouts;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->abouts = app(About::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $about = About::firstOrFail();
      return view('admin.abouts.index', compact('about'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

      $about = $this->abouts->firstOrFail();

      if(!$about) {
        session()->flash('messages.error', ['Quem Somos não existe!']);
        return redirect()->route('about.index');
      }

      if(!Input::hasFile('page_image') == null){
        $foto_antiga = $about->page_image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('page_image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('quem-somos');
        $about->page_image = $arquivo;
      }
      // testa depois      
      // $about->fill( $request->all() ); // o laravel ignora os elementos não atribuiveis

      $about->page_description = $request->page_description;
      $about->box_vision_title = $request->box_vision_title;
      $about->box_vision_description = $request->box_vision_description;
      $about->box_mission_title = $request->box_mission_title;
      $about->box_mission_description = $request->box_mission_description;
      $about->box_quality_one_title = $request->box_quality_one_title;
      $about->box_quality_one_description = $request->box_quality_one_description;
      $about->box_quality_two_title = $request->box_quality_two_title;
      $about->box_quality_two_description = $request->box_quality_two_description;
      $about->box_quality_three_title = $request->box_quality_three_title;
      $about->box_quality_three_description = $request->box_quality_three_description;
      $about->save();

      session()->flash('messages.success', ['Quem Somos alterado com sucesso!']);
      return redirect()->route('about.index');

    }

  }
