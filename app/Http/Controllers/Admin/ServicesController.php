<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Services\StoreRequest;
use App\Http\Requests\Admin\Services\UpdateRequest;

use App\Models\Service;

class ServicesController extends Controller
{
    /**
     * Armazena uma nova instancia do model Service
     *
     * @var \App\Service
     */
    private $services;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->services = app(Service::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $services = Service::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $service = new Service;

        $foto_antiga = $service->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('servicos');
        $service->image = $arquivo;


        $service->title = $request->title;
        $service->slug = $request->slug;
        $service->body = $request->body;

        if(!$service->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('services.index');
        }

        session()->flash('messages.success', ['Serviço cadastrado com sucesso!']);
        return redirect()->route('services.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.services.show', compact('service'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $service = $this->services->where('id', $id)->first();

        if(!$service) {
            session()->flash('messages.error', ['Serviço não existe!']);
            return redirect()->route('services.index');
        }

      if(!Input::hasFile('image') == null){
        $foto_antiga = $service->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('servicos');
        $service->image = $arquivo;
      }

        $service->title = $request->title;
        $service->slug = $request->slug;
        $service->body = $request->body;
        $service->save();

        session()->flash('messages.success', ['Serviço alterado com sucesso!']);
        return redirect()->route('services.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Request $request, Service $service)
  {

    $service->delete();

    return response(null, 204);
    //return response()->redirectToRoute('blogs.index');
  }
}
