<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Reservations\ReservationStoreRequest;
use App\Models\PreRegistration;
use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Reservations\StoreRequest;
use App\Http\Requests\Admin\Reservations\UpdateRequest;

use App\Models\Reservation;

class ReservationsController extends Controller
{
    /**
     * Armazena uma nova instancia do model Reservation
     *
     * @var Reservation
     */
    private $reservations;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->reservations = app(Reservation::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.reservations.index');
    }

	/**
	 * Inserir novo registro
	 *
	 * @param Request $request
	 */
    public function insert(Request $request)
	{
		/**
		 * Caso o formulário seja submetido cadastrar reserva.
		 */
		if($request->isMethod('post')) {

			// injetar validação de formulário
			app()->make(ReservationStoreRequest::class);

			$create = Reservation::create($request->all());

			if($create) {
				return redirect()->route('reservations.index');
			}
		}

		return view('admin.reservations.insert');
	}

	/**
	 * Visualizar informações da reserva ou atualizar
	 *
	 * @param Reservation $reservation
	 */
	public function viewOrUpdate(Request $request, Reservation $reservation)
	{
		if($request->isMethod('put')) {
			// injetar validação de formulário
			app()->make(ReservationStoreRequest::class);

			$reservation->update($request->all());

			return redirect()->back()->with('messages.success', ['Reserva cadastrada com sucesso!']);
		}

		return view('admin.reservations.view-update', compact('reservation'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $preRegistration = PreRegistration::findOrfail($id);
        $reservation = $preRegistration->reservation;
        return view('admin.reservations.create')->with(
            compact('preRegistration', 'reservation')
        );
    }

    /**
     * Cadastrar novo ou atualizar registro.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservationStoreRequest $request, $id)
    {
        /** @var PreRegistration $preRegistration */
        $preRegistration = PreRegistration::findOrFail($id);

        $campos = $request->except('name','email','phone','city', '_wysihtml5_mode', '_token');

        if(!$preRegistration->reservation) {
            // criar registro
            $preRegistration->reservation()->create($campos);
            return redirect()->back()->with('messages.success', ['Reserva cadastrada com sucesso!']);
        }else{
            // atualiza registro
            $preRegistration->reservation->update($campos);
            return redirect()->back()->with('messages.success', ['Reserva atualizada com sucesso!']);
        }
    }


	/**
	 * Remover registro do banco de dados
	 * @param Reservation $reservation
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
    public function delete(Reservation $reservation)
    {
        $reservation->delete();

        return response(null, 204);
    }
}
