<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Pages\StoreRequest;
use App\Http\Requests\Admin\Pages\UpdateRequest;

use App\Models\Page;

class PagesController extends Controller
{
    /**
     * Armazena uma nova instancia do model Page
     *
     * @var \App\Page
     */
    private $pages;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->pages = app(Page::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pages = Page::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $page = new Page;
        $page->title = $request->title;
        $page->url = $request->url;
        $page->body = $request->body;

        if(!$page->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('pages.index');
        }

        session()->flash('messages.success', ['Página cadastrada com sucesso!']);
        return redirect()->route('pages.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        return view('admin.pages.show', compact('page'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $page = $this->pages->where('id', $id)->first();

        if(!$page) {
            session()->flash('messages.error', ['Página não existe!']);
            return redirect()->route('pages.index');
        }

        $page->title = $request->title;
        $page->url = $request->url;
        $page->body = $request->body;
        $page->save();

        session()->flash('messages.success', ['Página alterada com sucesso!']);
        return redirect()->route('pages.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();
        return response()->redirectToRoute('pages.index');
    }
}
