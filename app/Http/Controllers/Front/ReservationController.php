<?php

namespace App\Http\Controllers\Front;

use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function index()
    {
    	return view('front.reservation');
    }

    public function view()
    {
    	return view('front.bank_reservation');
    }

	/**
	 * @param $bioma
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function show($bioma)
    {
    	$reservations = Reservation::where('bioma', 'like', '%'.$bioma.'%')->get();

    	return view('front.bank_reservation_show', compact('reservations', 'bioma'));
    }

	/**
	 * Obter informações da reserva selecionada
	 *
	 * @param $bioma
	 * @param Reservation $reservation
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function info($bioma, $id)
    {
		$reservation = Reservation::where('bioma', 'like', '%'.$bioma.'%')->where('id', $id)->firstOrFail();

    	return view('front.bank_reservation_info', compact('reservation'));
    }
}
