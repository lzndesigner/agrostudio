<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Blog;

class BlogsController extends Controller
{
	public function index()
	{
		return view('front.blogs');
	}

	public function show(Blog $blog)
	{
		return view('front.blogs_show', compact('blog'));
	}
}
