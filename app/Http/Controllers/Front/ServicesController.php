<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Service;

class ServicesController extends Controller
{
	public function index()
	{
		return view('front.services');
	}

	public function show(Service $service)
	{
		return view('front.services_show', compact('service'));
	}
}
