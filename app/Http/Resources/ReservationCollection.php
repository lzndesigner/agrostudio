<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
class ReservationCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'propietario' => $this->propietario,
            'fazenda' => $this->fazenda,
            'matricula' => $this->matricula,
            'municipio' => $this->municipio,
            'municipio_cartorio' => $this->municipio_cartorio,
            'estado' => $this->estado,
            'bioma' => $this->bioma,
            'situacao' => $this->situacao,
            'georeferencia' => $this->georeferencia,
            'inscricao_car' => $this->inscricao_car,
            'tamanho_area' => $this->tamanho_area,
            'area_tipo' => $this->area_tipo,
            'descricao' => $this->descricao,
            'ativo' => $this->ativo,
            'preRegistration' => $this->when($request->has('moreInfo'), $this->preRegistration),
			'interested' => $this->whenLoaded('interests', ReservationInterestCollection::collection($this->interests)),
            'details' => $this->whenLoaded('details', $this->details),
            'created_at' => $this->created_at->format('d-m-Y H:m'),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
