<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class NewsletterCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'status' => $this->when(true, function () {
                $st = null;
                switch ($this->status) {
                    case 'active': $st = 'Confirmou'; break;
                    case 'waiting': $st = 'Aguardando'; break;
                    case 'cancel': $st = 'Cancelou'; break;
                }
                return $st ?? $this->status;
            }),
            'not_formatted' => [
                'status' => $this->getOriginal('status')
            ],
            'created_at' => $this->created_at->format('d-m-Y H:m'),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
