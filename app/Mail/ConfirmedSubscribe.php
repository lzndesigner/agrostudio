<?php

namespace App\Mail;

use App\Models\Newsletter;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmedSubscribe extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Newsletter
     */
    private $newsletter;

	/**
	 * Create a new message instance.
	 *
	 * @param Newsletter $newsletter
	 */
    public function __construct(Newsletter $newsletter)
    {
        //
        $this->newsletter = $newsletter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Seja bem vindo')
			->from(config('mail.newsletter.address'), config('mail.newsletter.name'))
            ->to($this->newsletter->email, $this->newsletter->name)
            ->markdown('mail.site.newsletter_confirm', ['dados' => $this->newsletter]);
    }
}
