<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
    	'page_description',
    	'page_image',
    	'box_vision_title',
    	'box_vision_description',
    	'box_mission_title',
    	'box_mission_description',
    	'box_quality_one_title',
    	'box_quality_one_description',
    	'box_quality_two_title',
    	'box_quality_two_description',
    	'box_quality_three_title',
    	'box_quality_three_description'
    ];
}
