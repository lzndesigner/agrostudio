<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationDetail extends Model
{
    protected $fillable = [
    	'title',
    	'body',
    	'slug',
    	'image'
    ];

    /**
     * Informações da reserva
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reversation()
    {
        return $this->belongsTo(Reservation::class);
    }
}
