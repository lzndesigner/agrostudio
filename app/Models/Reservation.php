<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
    	'propietario',
        'fazenda',
        'matricula',
        'municipio',
        'municipio_cartorio',
        'estado',
        'bioma',
        'situacao', // [regular, irregular]
        'georeferencia', // [sim,nao]
        'inscricao_car',
        'tamanho_area',
        'area_tipo', // [Hectares,Alqueires]
        'descricao',
        'ativo'
    ];

    /**
     * Pre-cadastro relacionado à Reserva;
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function preRegistration()
    {
        return $this->belongsTo(PreRegistration::class);
    }

    /**
     * Detalhes publicos sobre a reserva
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function details()
    {
        return $this->hasOne(ReservationDetail::class);
    }

	/**
	 * Lista de interessados pela reserva
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function interests()
	{
		return $this->hasMany(ReservationInterest::class);
	}
}
