<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class PreRegistration extends Model
{
    /** @var array  */
    protected $fillable = ['name', 'phone', 'email', 'subject', 'city', 'state', 'note', 'status'];

    /**
     * Boot
     */
    public static function boot() {
        parent::boot();

        /**
         * Após cadastrar emitir notificação
         */
        self::created(function(PreRegistration $preApproval) {
            // notificar ao receber cadastro
            $preApproval->notify();
        });
    }

    /**
     * Obtem reserva
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reservation()
    {
        return $this->hasOne(Reservation::class);
    }

    /**
     * @param $key
     * @return null|string
     */
    public function getStatusAttribute($key)
    {
        $status = null;
        switch ($key) {
            case 'waiting': $status = 'aguardando'; break;
            case 'accept': $status = 'confirmado'; break;
            case 'reject': $status = 'recusado'; break;
        }

        return $status ?? $key;
    }

    /**
     * @param $key
     * @return null|string
     */
    public function getSubjectAttribute($key)
    {
        $subject = null;
        switch ($key) {
            case 'comprar': $subject = 'Comprar Reserva'; break;
            case 'anunciar': $subject = 'Anunciar Reserva'; break;
            case 'regularizar': $subject = 'Regularizar Reserva'; break;
        }

        return $subject ?? $key;
    }

    /**
     * Enviar notificação para email dos administradores
     *
     * @return mixed
     */
    public function notify() 
    {
    	$admin = \App\User::where('level', 'admin')->get();

    	if($admin->isNotEmpty()) {
    		Notification::send($admin, new \App\Notifications\PreRegisterReceived($this));
    	}
    }
}
