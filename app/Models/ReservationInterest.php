<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationInterest extends Model
{
    protected $fillable = ['name','email','phone','message','visualized'];

	/**
	 * Reserva relacionada ao cadastro de interesse
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function reservation()
	{
		return $this->belongsTo(Reservation::class);
	}
}
