<table>
    <thead>
        <tr>
            <th width="100">Nome Completo</th>
            <th width="100">E-mail</th>
        </tr>
    </thead>
    <tbody>
        @forelse($newsletters as $n)
            <tr style="background-color: #f1f1f1">
                <td>{{ $n->name }}</td>
                <td>{{ $n->email }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="2" style="text-align: center">Tabela vázia</td>
            </tr>
            @endforelse
    </tbody>
</table>