<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="{{ $configs->config_description ?? 'Somos referência em Regulamentação Florestal' }}">
  <meta name="keywords" content="{{ $configs->config_keywords ?? 'agrostudio, agro, studio' }}" />
  <title>{{ $configs->config_title ?? 'AgroStudio' }}</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('front/css/app.css?6') }}" type="text/css" />
  <!--[if lt IE 9]>
  <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="{{ asset('front/include/rs-plugin/css/settings.css') }}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ asset('front/include/rs-plugin/css/layers.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/include/rs-plugin/css/navigation.css') }}">
<style>
.section {
  z-index: 1;
}
.infoReservations ul{
  margin: 0;
  padding: 0;
  list-style: none;
}
.infoReservations ul li{
  display: list-item;
  font-size: 18px;
  padding: 15px 0 5px;
  border-bottom: 1px dashed #DDD;
}
.bankreservations h2 {
    font-size: 16px;
    text-align: center;
    margin: 5px 0;
}
.bankreservations img {
    width: 100%;
    height: 150px;
    border-radius: 5px;
}
@media screen and (max-width:981px){
  .bankreservations img {
      height: auto;
  }
}
</style>
@yield('cssPage')


</head>
<body class="stretched">
  <div id="app">
  <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">


    <!-- Header
      ============================================= -->
      <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">

        <div id="header-wrap">

          <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

          <!-- Logo
            ============================================= -->
            <div id="logo">
              <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('logo_branca.png') }}"><img src="{{ asset('logo_preto.png') }}" alt="AgroStudio"></a>
              <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('logo_branca.png') }}"><img src="{{ asset('logo_preto.png') }}" alt="AgroStudio"></a>
            </div><!-- #logo end -->

          <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">
              <!-- START SIDEBAR -->
              @include('front.includes.menu-top')
              <!-- END SIDEBAR -->
            </nav><!-- #primary-menu end -->

          </div>

        </div>

      </header><!-- #header end -->

      @yield('sliderPrincipal')

      @yield('breadcrumb')

  <!-- Content
    ============================================= -->
    <section id="content">
      <div class="content-wrap">
        @yield('content')
      </div><!-- .content-wrap -->
    </div><!-- #content -->
    <!-- End Content -->


  <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

      <div class="container">

        <div class="row">

        <!-- Footer Widgets
          ============================================= -->
          <div class="footer-widgets-wrap clearfix">

            <div class="col-xs-12 col-md-3">

              <div class="widget clearfix">
                <img src="{{ asset('logo_branca.png') }}" alt="AgroStudio" class="footer-logo">
                <p>{{$configs->config_description}}</p>
              </div>

            </div><!-- cols -->

            <div class="col-xs-12 col-md-3">
              <div class="widget widget_links clearfix">

                <h4>Navegação</h4>

                <ul>
                  <li><a href="{{ url('/') }}">Home</a></li>
                  <li><a href="{{ url('/quem-somos') }}">Quem Somos</a></li>
                  <li><a href="{{ url('/servicos') }}">Serviços</a></li>
                  <li><a href="{{ url('/blog') }}">Blog</a></li>
                  <li><a href="{{ url('/depoimentos') }}">Depoimentos</a></li>
                  <li><a href="{{ url('/contato') }}">Contato</a></li>
                </ul>

              </div>
            </div><!-- cols -->

            <div class="col-xs-12 col-md-3">
              <div class="widget clearfix">
                <h4>Artigos Recentes</h4>

                <div id="post-list-footer">

                  <div id="oc-articles" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="true" data-nav="false" data-autoplay="10000" data-pagi="true" data-items-xxs="1" data-items-xs="1" data-items-sm="1" data-items-md="1" data-items-lg="1" style="padding: 20px 0;">

                    <div class="oc-item">

                      <div class="spost clearfix">
                        <div class="entry-c">
                          <div class="entry-title">
                            <h4><a href="{{ url('/blog_read') }}">Lorem ipsum dolor sit amet, consectetur</a></h4>
                          </div>
                          <ul class="entry-meta">
                            <li>10th July 2014</li>
                          </ul>
                        </div>
                      </div>

                      <div class="spost clearfix">
                        <div class="entry-c">
                          <div class="entry-title">
                            <h4><a href="{{ url('/blog_read') }}">Elit Assumenda vel amet dolorum quasi</a></h4>
                          </div>
                          <ul class="entry-meta">
                            <li>10th July 2014</li>
                          </ul>
                        </div>
                      </div>

                    </div><!-- oc-item -->
                    <div class="oc-item">

                      <div class="spost clearfix">
                        <div class="entry-c">
                          <div class="entry-title">
                            <h4><a href="{{ url('/blog_read') }}">Lorem ipsum dolor sit amet, consectetur</a></h4>
                          </div>
                          <ul class="entry-meta">
                            <li>10th July 2014</li>
                          </ul>
                        </div>
                      </div>

                      <div class="spost clearfix">
                        <div class="entry-c">
                          <div class="entry-title">
                            <h4><a href="{{ url('/blog_read') }}">Elit Assumenda vel amet dolorum quasi</a></h4>
                          </div>
                          <ul class="entry-meta">
                            <li>10th July 2014</li>
                          </ul>
                        </div>
                      </div>

                    </div><!-- oc-item -->

                  </div><!-- carrousel -->

                </div>
              </div>
            </div><!-- cols -->

            <div class="col-xs-12 col-md-3">
              <h4>Entre em Contato</h4>
              <div style="background: url('{{ asset('front/images/world-map.png') }}') no-repeat center center; background-size: 100%;">
                <address>
                  <strong>Escritório:</strong><br>
                  {{$configs->company_address}}<br>
                </address>
                <abbr title="Telefone"><strong>Telefone:</strong></abbr> {{$configs->config_phone}} <br>
                @if(!empty($configs->config_cellphone))<abbr title="Celular"><strong>Celular:</strong></abbr> {{$configs->config_cellphone}} <br>@endif
                <abbr title="E-mail"><strong>E-mail:</strong></abbr> <a href="mailto:contato@agrostudio.com.br">contato@agrostudio.com.br</a>
              </div>
            </div><!-- cols -->



          </div><!-- .footer-widgets-wrap end -->

        </div><!-- row -->
      </div><!-- container -->

    <!-- Copyrights
      ============================================= -->
      <div id="copyrights">

        <div class="container clearfix">

          <div class="col_half">
            Todos os direitos reservados á <b>AgroStudio</b> {{ date('Y') }}
          </div>

          <div class="col_half col_last tright">
            <div class="fright clearfix">
              <a href="#" class="social-icon si-small si-borderless si-facebook">
                <i class="icon-facebook"></i>
                <i class="icon-facebook"></i>
              </a>

              <a href="#" class="social-icon si-small si-borderless si-twitter">
                <i class="icon-twitter"></i>
                <i class="icon-twitter"></i>
              </a>

              <a href="#" class="social-icon si-small si-borderless si-gplus">
                <i class="icon-gplus"></i>
                <i class="icon-gplus"></i>
              </a>

            </div>
          </div>

        </div>

      </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

  </div><!-- #wrapper end -->

<!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

</div>
<!-- External JavaScripts
  ============================================= -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/jquery.js') }}"></script>
  <script defer src="{{ asset('front/js/fontawesome-all.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/plugins.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/functions.js') }}"></script>
  @yield('jsPage')
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi8t-0tjEnDS6KlE5tYQUCf8szqfGOQGs&libraries=places&sensor=false"></script>
  <script type="text/javascript" src="{{ asset('front/js/jquery.gmap.js') }}"></script>
  <script type="text/javascript">
    jQuery('#google-map').gMap({
      address: '{{$configs->company_city}}, {{$configs->company_state}}, Brazil',
      maptype: 'ROADMAP',
      zoom: 14,
      markers: [
      {
        address: '{{$configs->company_city}}, {{$configs->company_state}}, Brazil',
        icon: {
          image: "{{ asset('front/images/icons/map-icon-red.png') }}",
          iconsize: [32, 39],
          iconanchor: [32,39]
        }
      }
      ],
      doubleclickzoom: false,
      controls: {
        panControl: true,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false
      }
    });
  </script>
</body>
</html>