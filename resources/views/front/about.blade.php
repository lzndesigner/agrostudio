@extends('front.base')
@section('title', 'Quem Somos')

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section class="section page-quem-somos">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading-block ">
          <h2 class="text-primary">Quem somos</h2>
          <h1>Conheça nossa história</h1>
        </div><!-- heading-block -->
      </div><!-- col-md- 12 -->

      <div class="col-md-12">
        <div class="col-md-7">

          {!! str_replace('{', ' ', $abouts->page_description) !!}

        </div><!-- col-md-7 -->
        <div class="col-md-5">
          <img src="/storage/{{ $abouts->page_image }}" alt="Quem Somos" class="img-responsive">
        </div><!-- col-md-5 -->
      </div><!-- col-md-12 -->

      <div class="col-md-12">
        <hr class="invisible">
        <div class="col-md-6 nobottommargin">
          <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
            <div class="fbox-icon">
              <a><i class="i-alt noborder icon-diamond"></i></a>
            </div>
            <h3>{{ $abouts->box_vision_title }}</h3>
            {{ $abouts->box_vision_description }}
          </div>
        </div><!-- col-md-6 -->
        <div class="col-md-6 nobottommargin">
          <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
            <div class="fbox-icon">
              <a><i class="i-alt noborder icon-rocket"></i></a>
            </div>
            <h3>{{ $abouts->box_mission_title }}</h3>
            {{ $abouts->box_mission_description }}
          </div>
        </div><!-- col-md-6 -->
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section>
<div class="clearfix"></div>
<section class="section section-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading-block ">
          <h1>Nossas Qualidades</h1>
        </div><!-- heading-block -->
      </div><!-- col-md- 12 -->
    </div><!-- row -->
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4 nobottommargin">
          <div class="feature-box fbox-light fbox-effect nobottomborder">
            <div class="fbox-icon">
              <a><i class="i-alt noborder icon-rocket"></i></a>
            </div>
            <h3>{{ $abouts->box_quality_one_title }}</h3>
            <p>{{ $abouts->box_quality_one_description }}</p>
          </div>
        </div><!-- col-md-4 -->
        <div class="col-md-4 nobottommargin">
          <div class="feature-box fbox-light fbox-effect nobottomborder">
            <div class="fbox-icon">
              <a><i class="i-alt noborder icon-rocket"></i></a>
            </div>
            <h3>{{ $abouts->box_quality_two_title }}</h3>
            <p>{{ $abouts->box_quality_two_description }}</p>
          </div>
        </div><!-- col-md-4 -->
        <div class="col-md-4 nobottommargin">
          <div class="feature-box fbox-light fbox-effect nobottomborder">
            <div class="fbox-icon">
              <a><i class="i-alt noborder icon-rocket"></i></a>
            </div>
            <h3>{{ $abouts->box_quality_three_title }}</h3>
            <p>{{ $abouts->box_quality_three_description }}</p>
          </div>
        </div><!-- col-md-4 -->
      </div>
    </div>
  </div><!-- container -->
</section>

<section class="section section-dark nomargin nopadding">
  <div class="container clearfix">

    <div id="oc-clients" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="true" data-nav="true" data-autoplay="5000" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="7" data-items-lg="8" style="padding: 20px 0;">

      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/1.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/2.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/3.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/4.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/5.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/6.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/7.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/8.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/9.png') }}" alt="Clients"></a></div>
      <div class="oc-item"><a href="http://logofury.com/"><img src="{{ asset('galerias/clientes/10.png') }}" alt="Clients"></a></div>

    </div>

  </div>
</section>
<!-- END CONTAINER -->
@endsection
