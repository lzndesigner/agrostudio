@extends('front.base')
@section('title', '')
@section('jsPage')
<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.video.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript">

var tpj=jQuery;
tpj.noConflict();

tpj(document).ready(function() {

 var apiRevoSlider = tpj('.tp-banner').show().revolution(
 {
   sliderType:"standard",
   jsFileLocation:"{{ asset('front/include/rs-plugin/js/') }}",
   sliderLayout:"fullscreen",
   dottedOverlay:"none",
   delay:9000,
   responsiveLevels:[1200,992,768,480,320],
   startwidth:1140,
   startheight:600,
   hideThumbs:200,

   thumbWidth:100,
   thumbHeight:50,
   thumbAmount:3,

   navigation: {
      keyboardNavigation:"off",
      keyboard_direction: "horizontal",
      mouseScrollNavigation:"off",
      onHoverStop:"off",
      touch:{
         touchenabled:"on",
         swipe_threshold: 75,
         swipe_min_touches: 1,
         swipe_direction: "horizontal",
         drag_block_vertical: false
     },
     arrows: {
         style: "hermes",
         enable: true,
         hide_onmobile: false,
         hide_onleave: false,
         tmp: '<div class="tp-arr-allwrapper"> <div class="tp-arr-imgholder"></div></div>',
         left: {
            h_align: "left",
            v_align: "center",
            h_offset: 10,
            v_offset: 0
        },
        right: {
            h_align: "right",
            v_align: "center",
            h_offset: 10,
            v_offset: 0
        }
    }
},

touchenabled:"on",
onHoverStop:"on",

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,


parallax:"mouse",
parallaxBgFreeze:"on",
parallaxLevels:[8,7,6,5,4,3,2,1],
parallaxDisableOnMobile:"on",

keyboardNavigation:"on",

shadow:0,
fullWidth:"off",
fullScreen:"on",

spinner:"spinner0",

stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,

shuffle:"off",


forceFullWidth:"off",
fullScreenAlignForce:"off",
minFullScreenHeight:"400",

hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"off",
hideThumbsUnderResolution:0,

hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,
fullScreenOffset:"0px"
});

apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
    setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
});

apiRevoSlider.bind("revolution.slide.onchange",function (e,data) {
    SEMICOLON.slider.revolutionSliderMenu();
});

		}); //END REVOLUTION SLIDER

</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.gmap.js') }}"></script>
<script type="text/javascript">

jQuery('#google-map').gMap({
 address: '{{$configs->company_city}}, {{$configs->company_state}}, Brazil',
 maptype: 'ROADMAP',
 zoom: 14,
 markers: [
 {
   address: "{{$configs->company_city}}, {{$configs->company_state}}, Brazil",
   icon: {
      image: "{{ asset('front/images/icons/map-icon-red.png') }}",
      iconsize: [32, 39],
      iconanchor: [32,39]
  }
}
],
doubleclickzoom: false,
controls: {
    panControl: true,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: false,
    streetViewControl: false,
    overviewMapControl: false
}
});

</script>
@endsection
@section('sliderPrincipal')
@include('front.includes.slider-principal')
@endsection

@section('content')
<!-- START CONTAINER -->

<section class="section">
    <div class="container">

        <div class="heading-block ">
            <h2>Banco de Reservas</h2>
        </div>

        <div class="row bankreservations">
            <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'amazonia']) }}">
                <img src="{{ asset('storage/reservas/tipos/amazonia.jpg') }}" alt="Amazônia" class="img-responsive"><h2>Amazônia</h2></a>
            </div>
            <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'caatinga']) }}">
                <img src="{{ asset('storage/reservas/tipos/caatinga.jpg') }}" alt="Caatinga" class="img-responsive"><h2>Caatinga</h2></a>
            </div>
            <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'cerrado']) }}">
                <img src="{{ asset('storage/reservas/tipos/cerrado.jpg') }}" alt="Cerrado" class="img-responsive"><h2>Cerrado</h2></a>
            </div>
            <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'mataatlantica']) }}">
                <img src="{{ asset('storage/reservas/tipos/mataatlantica.jpg') }}" alt="Mata Atlântica" class="img-responsive"><h2>Mata Atlântica</h2></a>
            </div>
            <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'pampa']) }}">
                <img src="{{ asset('storage/reservas/tipos/pampa.jpg') }}" alt="Pampa" class="img-responsive"><h2>Pampa</h2></a>
            </div>
            <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'pantanal']) }}">
                <img src="{{ asset('storage/reservas/tipos/pantanal.jpg') }}" alt="Pantanal" class="img-responsive"><h2>Pantanal</h2></a>
            </div>
        </div>
    </div><!-- container -->
</section>

<div class="clearfix"></div>

<section class="section">
    <div class="container clearfix">
        <div class="divcenter center clearfix">
            <h2>Cadastre seu e-mail e fique por dentro das novidades!</h2>
            <form-newsletter></form-newsletter>
            <div class="sombra-div"></div>
        </div>
    </div><!-- container -->
</section>

<div class="clearfix"></div>

<section class="section">
    <div class="container">

        <div class="heading-block ">
            <h2>Nossos principais serviços</h2>
            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.</span>
        </div>


        <div id="oc-blog" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="@if(count($services) >= 3)  true  @else  false @endif" data-nav="false" data-autoplay="10000" data-pagi="true" data-items-xxs="1" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="3" style="padding: 20px 0;">

            @foreach($services as $service)
            <div class="oc-item">
                <div class="feature-box media-box">
                    <div class="fbox-media">
                        <a href="/servicos/{{ $service->slug }}"><img src="/storage/{{ $service->image ?? old('image') }}" alt="{{ $service->title }}"></a>
                    </div>
                    <div class="fbox-desc">
                        <h3><a href="/servicos/{{ $service->slug }}">{{ $service->title }}</a></h3>
                        <p>{!! str_limit($service->body, $limit = 190, $end = '...') !!}</p>
                    </div>
                </div>
            </div><!-- col -->
            @endforeach


        </div><!-- row -->

    </div><!-- container -->
</section>

<div class="clearfix" id="quem-somos"></div>

<section class="section page-quem-somos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-block ">
                    <h2 class="text-primary">Quem somos</h2>
                    <h1>Conheça nossa história</h1>
                </div><!-- heading-block -->
            </div><!-- col-md- 12 -->

            <div class="col-md-12">
                <div class="col-md-7">

                    {!! $abouts->page_description !!}

                </div><!-- col-md-7 -->
                <div class="col-md-5">
                    <img src="/storage/{{ $abouts->page_image }}" alt="Quem Somos" class="img-responsive">
                </div><!-- col-md-5 -->
            </div><!-- col-md-12 -->

            <div class="col-md-12">
                <hr class="invisible">
                <div class="col-md-6 bottommargin">
                    <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                        <div class="fbox-icon">
                            <a><i class="i-alt noborder icon-diamond"></i></a>
                        </div>
                        <h3>{{ $abouts->box_vision_title }}</h3>
                        {{ $abouts->box_vision_description }}
                    </div>
                </div><!-- col-md-6 -->
                <div class="col-md-6 bottommargin">
                    <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                        <div class="fbox-icon">
                            <a><i class="i-alt noborder icon-rocket"></i></a>
                        </div>
                        <h3>{{ $abouts->box_mission_title }}</h3>
                        {{ $abouts->box_mission_description }}
                    </div>
                </div><!-- col-md-6 -->
            </div>
        </div><!-- row -->
    </div><!-- container -->
</section>

<div class="clearfix"></div>

<section class="section topmargin-sm nomargin background-depoimentos-clientes">

    <h1 class="uppercase center">Veja o que nossos <span>Clientes</span> dizem</h1>

    <div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
        <div class="flexslider">
            <div class="slider-wrap">
                @foreach($reviews as $review)
                <div class="slide">
                    <div class="testi-content">
                        <p>{{ $review->body }}</p>
                        <div class="testi-meta">
                            {{ $review->name }}
                            <span>{{ $review->charge }}</span>
                        </div>
                    </div>
                </div><!-- slide -->
                @endforeach

            </div>
        </div>
    </div><!-- fslider -->

</section>

<div class="clearfix"></div>

<section class="section section-dark nomargin nopadding">
    <div class="container clearfix">

        <div id="oc-clients" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="@if(count($clients) >= 4)  true  @else  false @endif" data-nav="true" data-autoplay="5000" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="7" data-items-lg="8" style="padding: 20px 0;">

            @foreach($clients as $client)
            <div class="oc-item"><a href="{{ $client->link }}" target="_Blank" data-toggle="tooltip" title="{{ $client->name }}"><img src="/storage/{{ $client->image }}" alt="{{ $client->name }}"></a></div>
            @endforeach

        </div>

    </div>
</section>

<div class="clearfix"></div>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-block ">
                    <h1 class="text-primary">Nosso Blog</h1>
                </div><!-- heading-block -->
            </div><!-- col-md- 12 -->
        </div><!-- row -->

        <div id="oc-blog" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="@if(count($blogs) >= 3)  true  @else  false @endif" data-nav="false" data-autoplay="10000" data-pagi="true" data-items-xxs="1" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="3" style="padding: 20px 0;">


            @foreach($blogs as $blog)
            <div class="oc-item">
                <div class="feature-box media-box">
                    <div class="fbox-media">
                        <a href="/blogs/{{ $blog->slug }}"><img src="storage/{{ $blog->image }}" alt="{{ $blog->title }}"></a>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li><i class="icon-calendar3"></i> {{ $blog->created_at }}</li>
                    </ul>
                    <div class="fbox-desc">
                        <h3><a href="/blogs/{{ $blog->slug }}">{{ $blog->title }}</a></h3>
                        <p>{!! str_limit($blog->body, $limit = 190, $end = '...') !!}</p>
                    </div>
                </div>
            </div><!-- oc-item -->
            @endforeach

        </div><!-- carrousel -->


    </div><!-- container -->
</section>

<div class="clearfix"></div>

<section class="section section-primary section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-block ">
                    <h1>Nossas Qualidades</h1>
                </div><!-- heading-block -->
            </div><!-- col-md- 12 -->
        </div><!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 bottommargin">
                    <div class="feature-box fbox-light fbox-effect nobottomborder">
                        <div class="fbox-icon">
                            <a><i class="i-alt noborder icon-rocket"></i></a>
                        </div>
                        <h3>{{ $abouts->box_quality_one_title }}</h3>
                        <p>{{ $abouts->box_quality_one_description }}</p>
                    </div>
                </div><!-- col-md-4 -->
                <div class="col-md-4 bottommargin">
                    <div class="feature-box fbox-light fbox-effect nobottomborder">
                        <div class="fbox-icon">
                            <a><i class="i-alt noborder icon-rocket"></i></a>
                        </div>
                        <h3>{{ $abouts->box_quality_two_title }}</h3>
                        <p>{{ $abouts->box_quality_two_description }}</p>
                    </div>
                </div><!-- col-md-4 -->
                <div class="col-md-4 bottommargin">
                    <div class="feature-box fbox-light fbox-effect nobottomborder">
                        <div class="fbox-icon">
                            <a><i class="i-alt noborder icon-rocket"></i></a>
                        </div>
                        <h3>{{ $abouts->box_quality_three_title }}</h3>
                        <p>{{ $abouts->box_quality_three_description }}</p>
                    </div>
                </div><!-- col-md-4 -->
            </div>
        </div>
    </div><!-- container -->
</section>

<div class="clearfix"></div>


<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">

                <div class="fancy-title title-dotted-border">
                    <h3>Entre em Contato</h3>
                </div>

                <div class="contact-widget">

                    <div class="contact-form-result"></div>

                    <form-contato></form-contato>
                </div><!-- contact-widget -->

            </div><!-- col-xs-12 col-md-6 -->

            <div class="col-xs-12 col-md-4 center">
                <img src="{{ asset('galerias/paginas/escritorio.png')}}" alt="Escritório" class="thumbnail ">
            </div>
        </div><!-- row -->

        <div class="clearfix"></div>

        <div class="row clear-bottommargin">
            <div class="col-xs-12 col-md-3 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a><i class="icon-phone3"></i></a>
                    </div>
                    <h3>Telefone<span class="subtitle">{{$configs->config_phone}} @if(!empty($configs->config_cellphone))<br> {{$configs->config_cellphone}} @endif</h3>
                </div>
            </div>

            <div class="col-xs-12 col-md-3 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a><i class="icon-envelope"></i></a>
                    </div>
                    <h3>E-mail<span class="subtitle"><a href="mailto:{{$configs->config_email}}">{{$configs->config_email}}</a></span></h3>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a><i class="icon-map-marker2"></i></a>
                    </div>
                    <h3>Escritório<span class="subtitle">{{$configs->company_address}}</span></h3>
                </div>
            </div>
        </div>
    </div><!-- container -->
</section>

@include('components.mapa')

<!-- END CONTAINER -->
@endsection