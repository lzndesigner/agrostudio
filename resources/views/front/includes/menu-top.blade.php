<ul>
  <li class=""><a href="{{ url('/') }}"><div>Home</div></a></li>
  <li class=""><a href="{{ url('/quem-somos') }}"><div>Quem Somos</div></a></li>
  <li class=""><a href="{{ url('/servicos') }}"><div>Serviços</div></a>
    @if(count($services) >= 1)
  	<ul>
      @foreach($services as $service)
  		<li><a href="/servicos/{{ $service->slug }}">{{ $service->title }}</a></li>
      @endforeach
  	</ul>
    @endif
  </li>
  <li class=""><a href="{{ url('/reservas') }}"><div>Reservas</div></a>
    <ul>
      <li><a href="{{ url('/reservas') }}">Cadastrar Reservas</a></li>
      <li><a href="{{ url('/banco-reservas') }}">Buscar Reservas</a></li>
    </ul>
  </li>
  <li class=""><a href="{{ url('/blogs') }}"><div>Blog</div></a></li>
  <li class=""><a href="{{ url('/depoimentos') }}"><div>Depoimentos</div></a></li>
  <li class=""><a href="{{ url('/contato') }}"><div>Contato</div></a></li>
</ul>