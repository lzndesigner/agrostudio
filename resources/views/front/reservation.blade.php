@extends('front.base')
@section('title', 'Anuncie sua Reserva')
@section('jsPage')
@endsection

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="fancy-title title-dotted-border">
          <h3>Anuncie sua Reserva</h3>
        </div>

        <div class="contact-widget">
          <form-reserva token="{{ getenv('GEOCODE_API_JS') }}"></form-reserva>
        </div><!-- contact-widget -->

      </div><!-- col-xs-12 col-md-6 -->

      <div class="col-xs-12 col-md-6">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi temporibus sapiente qui voluptatibus excepturi nostrum delectus omnis soluta est assumenda ipsum cupiditate quo earum quam, suscipit, numquam fugiat quia voluptates.</p>
        <img src="{{ asset('storage/reservas/explicacao_2.png')}}" alt="Reserva Legal" class="">
      </div>

    </div><!-- row -->

    <div class="clearfix"></div>

    <!-- END CONTAINER -->
    @endsection
