@extends('front.base')
@section('title', 'Nossos Serviços')

@section('content')
<!-- START CONTAINER -->
<section class="section">
  <div class="container">

    <div class="heading-block ">
      <h2>Nossos principais serviços</h2>
      <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.</span>
    </div>

    <div class="row">

    @foreach($services as $service)
    <div class="col-xs-12 col-md-4 bottommargin-sm">
      <div class="feature-box media-box">
        <div class="fbox-media">
          <a href="/servicos/{{ $service->slug }}"><img src="/storage/{{ $service->image }}" alt="{{ $service->title }}"></a>
        </div>
        <div class="fbox-desc">
          <h3><a href="/servicos/{{ $service->slug }}">{{ $service->title }}</a></h3>
          {!! str_limit($service->body, $limit = 190, $end = '...') !!}
        </div>
      </div>
    </div><!-- col -->
    @endforeach


    </div>

  </div><!-- container -->
</section>
<!-- END CONTAINER -->
@endsection
