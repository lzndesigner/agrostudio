@extends('admin.base')
@section('title', 'Páginas de Reservas Cadastradas')

@section('content')


  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
  <!-- End Page Header -->


  <!-- START CONTAINER -->
  <div class="container-default" id="app">
    @include('elements.messages')
    <div class="container-padding">
      <div class="row">
        <div class="col-md-12">
          <manage-reservations></manage-reservations>
        </div><!-- col-md-12 -->
      </div><!-- row -->
    </div><!-- container-padding -->

  </div><!-- container-default -->
  <!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
@endsection