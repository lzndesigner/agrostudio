@extends('admin.base')
@section('title', 'Gerenciar Newsletter')

@section('content')
    <!-- Start Page Header -->
    <div class="page-header">
        <h1 class="title">@yield('title')</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li class="active">@yield('title')</li>
        </ol>
    </div>
    <!-- End Page Header -->


    <!-- START CONTAINER -->
    <div class="container-default" id="app">
        <div class="container-padding">
            <manager-newsletter></manager-newsletter>
        </div>
    </div><!-- container-default -->
    <!-- END CONTAINER -->
@endsection
