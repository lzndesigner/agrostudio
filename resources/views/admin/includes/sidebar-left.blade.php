<div class="sidebar clearfix">

    <ul class="sidebar-panel nav">
        <li class="sidetitle">NAVEGAÇÃO</li>
        <li><a href="{{ url('/') }}" target="_Blank"><span class="icon color5"><i class="fa fa-desktop"></i></span>Meu Site</a></li>
        <li><a href="{{ url('/dashboard') }}"><span class="icon color5"><i class="fa fa-home"></i></span>Painel de Controle</a></li>

        <li><a href="#"><span class="icon color7"><i class="fa fa-cog"></i></span>Configuração<span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('configs.index') }}">Geral</a></li>
            </ul>
        </li>
        <li><a href="{{ route('users.index') }}"><span class="icon color5"><i class="fa fa-user"></i></span>Usuários Administrativos</a></li>
        <li><a href="{{ route('sliders.index') }}"><span class="icon color5"><i class="fa fa-image"></i></span>Sliders</a></li>

        <li>
            <a href="{{ route('newsletter.index') }}">
                <span class="icon color5"><i class="fa fa-send"></i></span> Newsletter
            </a>
        </li>

        <li><a href="#"><span class="icon color7"><i class="fa fa-tree"></i></span>Reservas<span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('pre-registrations.index') }}"><i class="fa fa-users"></i> Interessados</a></li>
                <li><a href="{{ route('reservations.index') }}"><i class="fa fa-flag"></i> Reservas</a></li>
                <li><a href="{{ route('reservations.insert') }}"><i class="fa fa-plus"></i> Cadastrar</a></li>
            </ul>
        </li>

        <li><a href="#"><span class="icon color7"><i class="fa fa-book"></i></span>Páginas<span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('blogs.index') }}"><i class="fa fa-comments"></i> Blog</a></li>
                <li><a href="{{ route('about.index') }}"><i class="fa fa-heart"></i> Quem Somos</a></li>
                <li><a href="{{ route('services.index') }}"><i class="fa fa-cog"></i> Serviços</a></li>
                <li><a href="{{ route('reviews.index') }}"><i class="fa fa-heart"></i> Depoimentos</a></li>
                <li><a href="{{ route('clients.index') }}"><i class="fa fa-users"></i> Clientes/Parceiros</a></li>
                <li><a href="{{ route('pages.index') }}"><i class="fa fa-book"></i> Página de Informações</a></li>
            </ul>
        </li>
    </ul>

    <ul class="sidebar-panel nav">
        <li class="sidetitle">OUTROS</li>
        <li><a href="{{ url('https://www.innsystem.com.br/central-ajuda') }}" target="_Blank"><span class="icon color15"><i class="fa fa-bank"></i></span>Central de Ajuda</a></li>
        <li><a href="{{ url('https://www.innsystem.com.br/contato') }}" target="_Blank"><span class="icon color15"><i class="fa fa-tags"></i></span>Ticket de Suporte</a></li>
    </ul>

</div><!-- sidebar -->