@extends('admin.base')
@section('title', 'Detalhes do Slider')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $slider->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('sliders.index') }}">Página de Sliders</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End slider Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $slider->title }}</h2>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $slider->image ?? old('image') }}" alt="Sliders" class="img-thumbnail">

            <p><small>Criado em {{ $slider->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $slider->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $slider->body !!}</blockquote>

            <hr>

            <a href="{{ route('sliders.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('sliders.edit', $slider->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('sliders.destroy', $slider->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-sliders').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-sliders" action="{{ route('sliders.destroy', $slider->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection