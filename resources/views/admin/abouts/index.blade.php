@extends('admin.base')
@section('title', 'Editar Quem Somos')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
           @yield('title')
         </div>

         <div class="panel-body">

          <form action="{{ route('about.store')}}" enctype="multipart/form-data" method="POST">
            {!! csrf_field() !!}

            <div class="row">
              <div class="col-xs-12 col-md-8">

                <div class="form-group @if ($errors->has('page_description')) has-error @endif">
                  <label for="page_description" class="form-label">Descrição da Página</label>
                  <textarea rows="18" class="form-control textarea" id="page_description" name="page_description" value="{{ $abouts->page_description ?? old('page_description') }}" placeholder="Descrição da Página" autofocus>{{ $abouts->page_description ?? old('page_description') }}</textarea>
                  @if ($errors->has('page_description'))
                  <span class="help-block">
                    <strong>{{ $errors->first('page_description') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->

              </div>
              <div class="col-xs-12 col-md-4">
                <div class="form-group">
                  <label for="page_image" class="form-label">Imagem da Página</label>
                  <img src="/storage/{{ $abouts->page_image ?? old('page_image') }}" alt="Quem Somos" class="img-thumbnail">
                  <input type="file" class="form-control" id="page_image" name="page_image" value="{{ $abouts->page_image ?? old('page_image') }}">
                </div>

                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
              </div>
            </div>

            <hr>

            <div class="row">
              <div class="col-xs-12 col-md-5 col-md-offset-1">
                <h4>Box  <b>Nossa Visão</b></h4>

                <div class="form-group @if ($errors->has('box_vision_title')) has-error @endif">
                  <label for="box_vision_title" class="form-label">Título</label>
                  <input type="text" class="form-control" id="box_vision_title" name="box_vision_title" value="{{ $abouts->box_vision_title ?? old('box_vision_title') }}" placeholder="Título da Box">
                  @if ($errors->has('box_vision_title'))
                  <span class="help-block">
                    <strong>{{ $errors->first('box_vision_title') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->

                <div class="form-group @if ($errors->has('box_vision_description')) has-error @endif">
                  <label for="box_vision_description" class="form-label">Descrição</label>
                  <textarea rows="4" class="form-control" id="box_vision_description" name="box_vision_description" value="{{ $abouts->box_vision_description ?? old('box_vision_description') }}" placeholder="Descrição da Página">{{ $abouts->box_vision_description ?? old('box_vision_description') }}</textarea>
                  @if ($errors->has('box_vision_description'))
                  <span class="help-block">
                    <strong>{{ $errors->first('box_vision_description') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->

              </div><!-- col-md-5 -->
              <div class="col-xs-12 col-md-5">
                <h4>Box  <b>Nossa Missão</b></h4>

                <div class="form-group @if ($errors->has('box_mission_title')) has-error @endif">
                  <label for="box_mission_title" class="form-label">Título</label>
                  <input type="text" class="form-control" id="box_mission_title" name="box_mission_title" value="{{ $abouts->box_mission_title ?? old('box_mission_title') }}" placeholder="Título da Box">
                  @if ($errors->has('box_mission_title'))
                  <span class="help-block">
                    <strong>{{ $errors->first('box_mission_title') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->

                <div class="form-group @if ($errors->has('box_mission_description')) has-error @endif">
                  <label for="box_mission_description" class="form-label">Descrição</label>
                  <textarea rows="4" class="form-control" id="box_mission_description" name="box_mission_description" value="{{ $abouts->box_mission_description ?? old('box_mission_description') }}" placeholder="Descrição da Página">{{ $abouts->box_mission_description ?? old('box_mission_description') }}</textarea>
                  @if ($errors->has('box_mission_description'))
                  <span class="help-block">
                    <strong>{{ $errors->first('box_mission_description') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->

              </div><!-- col-md-5 -->
            </div><!-- row -->

            <hr>

            <div class="row">
              <div class="col-xs-12 col-md-10 col-md-offset-1">
                <h4>Box  <b>Nossa Qualidades</b></h4>

                <div class="row">
                  <div class="col-xs-12 col-md-4">
                    <div class="form-group @if ($errors->has('box_quality_one_title')) has-error @endif">
                      <label for="box_quality_one_title" class="form-label">Título</label>
                      <input type="text" class="form-control" id="box_quality_one_title" name="box_quality_one_title" value="{{ $abouts->box_quality_one_title ?? old('box_quality_one_title') }}" placeholder="Título da Box">
                      @if ($errors->has('box_quality_one_title'))
                      <span class="help-block">
                        <strong>{{ $errors->first('box_quality_one_title') }}</strong>
                      </span>
                      @endif
                    </div><!-- form-group -->

                    <div class="form-group @if ($errors->has('box_quality_one_description')) has-error @endif">
                      <label for="box_quality_one_description" class="form-label">Descrição</label>
                      <textarea rows="6" class="form-control" id="box_quality_one_description" name="box_quality_one_description" value="{{ $abouts->box_quality_one_description ?? old('box_quality_one_description') }}" placeholder="Descrição da Página">{{ $abouts->box_quality_one_description ?? old('box_quality_one_description') }}</textarea>
                      @if ($errors->has('box_quality_one_description'))
                      <span class="help-block">
                        <strong>{{ $errors->first('box_quality_one_description') }}</strong>
                      </span>
                      @endif
                    </div><!-- form-group -->
                  </div><!-- col-md-4 -->

                  <div class="col-xs-12 col-md-4">
                    <div class="form-group @if ($errors->has('box_quality_two_title')) has-error @endif">
                      <label for="box_quality_two_title" class="form-label">Título</label>
                      <input type="text" class="form-control" id="box_quality_two_title" name="box_quality_two_title" value="{{ $abouts->box_quality_two_title ?? old('box_quality_two_title') }}" placeholder="Título da Box">
                      @if ($errors->has('box_quality_two_title'))
                      <span class="help-block">
                        <strong>{{ $errors->first('box_quality_two_title') }}</strong>
                      </span>
                      @endif
                    </div><!-- form-group -->

                    <div class="form-group @if ($errors->has('box_quality_two_description')) has-error @endif">
                      <label for="box_quality_two_description" class="form-label">Descrição</label>
                      <textarea rows="6" class="form-control" id="box_quality_two_description" name="box_quality_two_description" value="{{ $abouts->box_quality_two_description ?? old('box_quality_two_description') }}" placeholder="Descrição da Página">{{ $abouts->box_quality_two_description ?? old('box_quality_two_description') }}</textarea>
                      @if ($errors->has('box_quality_two_description'))
                      <span class="help-block">
                        <strong>{{ $errors->first('box_quality_two_description') }}</strong>
                      </span>
                      @endif
                    </div><!-- form-group -->
                  </div><!-- col-md-4 -->

                  <div class="col-xs-12 col-md-4">
                    <div class="form-group @if ($errors->has('box_quality_three_title')) has-error @endif">
                      <label for="box_quality_three_title" class="form-label">Título</label>
                      <input type="text" class="form-control" id="box_quality_three_title" name="box_quality_three_title" value="{{ $abouts->box_quality_three_title ?? old('box_quality_three_title') }}" placeholder="Título da Box">
                      @if ($errors->has('box_quality_three_title'))
                      <span class="help-block">
                        <strong>{{ $errors->first('box_quality_three_title') }}</strong>
                      </span>
                      @endif
                    </div><!-- form-group -->

                    <div class="form-group @if ($errors->has('box_quality_three_description')) has-error @endif">
                      <label for="box_quality_three_description" class="form-label">Descrição</label>
                      <textarea rows="6" class="form-control" id="box_quality_three_description" name="box_quality_three_description" value="{{ $abouts->box_quality_three_description ?? old('box_quality_three_description') }}" placeholder="Descrição da Página">{{ $abouts->box_quality_three_description ?? old('box_quality_three_description') }}</textarea>
                      @if ($errors->has('box_quality_three_description'))
                      <span class="help-block">
                        <strong>{{ $errors->first('box_quality_three_description') }}</strong>
                      </span>
                      @endif
                    </div><!-- form-group -->
                  </div><!-- col-md-4 -->

                </div><!-- row -->
                

              </div><!-- col-md-11 -->

              <div class="row">
                <div class="col-md-6">
                  <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
                  <a href="{{ url('/dashboard') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Cancelar</a>
                </div><!-- col-md-6 -->
              </div>

            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')

@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js"></script>

<script>
/* BOOTSTRAP WYSIHTML5 */
$('.textarea').wysihtml5();

/* SUMMERNOTE*/
$(document).ready(function() {
  $('.summernote').summernote();
});
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
$('input[name="title"]').stringToSlug({
  setEvents: 'keyup keydown blur',
  getPut: 'input[name="url"]',
  space: '-',
  replace: '/\s?\([^\)]*\)/gi',
AND: 'e'
});
</script>
@endsection